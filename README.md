# Prerequisites

[Download](http://www.erlang.org/downloads) Erlang

[Download](https://www.rabbitmq.com/download.html) RabbitMQ

[Configure](https://www.rabbitmq.com/web-stomp.html) RabbitMQ

* ```> rabbitmq-plugins enable rabbitmq_management``` (allow web management)
* ```> rabbitmq-plugins enable rabbitmq_web_stomp``` (expose WS with Stomp plugin)

# Notes

RMQ web management console: http://127.0.0.1:15672/#/

RMQ WS: http://127.0.0.1:15674/ws

# Run

```> npm run angular-server```

```> npm run ws-server```

or both

```> npm run serve```

Machine events producer

```> node simple_rmq_machine_sensor_emulator.js```
