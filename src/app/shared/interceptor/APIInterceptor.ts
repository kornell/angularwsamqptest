import {Injectable, Injector} from '@angular/core';
import {
  HttpErrorResponse,
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
  HttpResponse
} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/do';
import {AuthService} from '../service/AuthService';

@Injectable()
export class APIInterceptor implements HttpInterceptor {

  private authService: AuthService;

  constructor(private injector: Injector) {
    setTimeout(() => {
      this.authService = this.injector.get(AuthService);
    });
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const apiReq = req.clone({
      url: 'http://' + document.location.hostname + ':8090' + req.url,
      withCredentials: false
    });
    return next.handle(apiReq)/*.do((event: HttpEvent<any>) => {
      if (event instanceof HttpResponse) {
        if (req.url !== '/login' && /login$/.test((<HttpResponse<any>>event).url)) {
          return this.authService.logout();
        }
      }
    }, (err: any) => {
      if (err instanceof HttpErrorResponse) {
        if (/login$/.test(err.url)) {
          return this.authService.logout();
        }
      }
    })*/;
  }

}
