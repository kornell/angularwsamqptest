import {EventEmitter, Injectable} from '@angular/core';
import 'rxjs/add/operator/map';
import {MatSnackBar} from '@angular/material';
import {TranslateService} from 'ng2-translate';

export type AuthEvent = 'verificationSuccess' | 'verificationFailure';

@Injectable()
export class AuthService {

  public onAuthEvent: EventEmitter<AuthEvent>;

  constructor(private snackBar: MatSnackBar,
              private t: TranslateService) {
    this.onAuthEvent = new EventEmitter();

    this.verify();
  }

  /**
   * Fake auth implementation
   */
  verify(): void {
    setTimeout(() => {
      this.snackBar.open(
        this.t.instant('snack.fake-login'),
        this.t.instant('snack.close'),
        {duration: 1000}
      );
      this.onAuthEvent.emit('verificationSuccess');
    }, 1000);
  }

}
