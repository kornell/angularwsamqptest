import {Subject} from 'rxjs/Subject';
import {Injectable} from '@angular/core';
import {StompConfig, StompRService, StompState} from '@stomp/ng2-stompjs';
import {Message} from '@stomp/stompjs';
import {Subscription} from 'rxjs/Subscription';
import {Observable} from 'rxjs/Observable';

export class MachineTemperature {

  name: string;

  temperature: number;

}

export class MachineAlarm {

  name: string;

  temperature: number;

  limit: number;

  state: 'ERROR' | 'OK';

}

@Injectable()
export class WebSocketRabbitMQService {

  public queue: Subject<MachineTemperature>;

  public queueAlarms: Subject<MachineAlarm>;

  private temperatureSubscription: Subscription;

  constructor(private stompService: StompRService) {
    this.queue = new Subject<MachineTemperature>();
    this.queueAlarms = new Subject<MachineAlarm>();
  }

  public connect(): void {

    // Listen to RabbitMQ connection state
    this.stompService.state
      .map((state: number) => StompState[state])
      .subscribe((status: string) => {
        console.info(`Stomp connection status: ${status}`);

        if (status === 'CONNECTED') {
          // Create queue subscriber
          const topicListener: Observable<Message> = this.stompService.subscribe('temperature');
          this.temperatureSubscription = topicListener.map((message: Message) => {
            return message.body;
          }).subscribe((messageBody: string) => {
            const messageJson: any = JSON.parse(messageBody);
            // Emit event on new message
            // This works as a simple dispatcher
            switch (messageJson.type) {
              case 'machineValueChange':
                this.publishTemperature(<MachineTemperature>messageJson.message);
                break;
              case 'machineValueAlarm':
                this.publishAlarm(<MachineAlarm>messageJson.message);
                break;
            }
            // console.log(`Received: ${JSON.stringify(messageJson, null, 2)}`);
          });
        } else if (this.temperatureSubscription) {
          this.temperatureSubscription.unsubscribe();
        }
      });

    const stompConfig: StompConfig = {
      // Which server?
      url: 'ws://127.0.0.1:15674/ws',

      // Headers
      // Typical keys: login, passcode, host
      headers: {
        login: 'guest',
        passcode: 'guest'
      },

      // How often to heartbeat?
      // Interval in milliseconds, set to 0 to disable
      heartbeat_in: 0, // Typical value 0 - disabled
      heartbeat_out: 20000, // Typical value 20000 - every 20 seconds

      // Wait in milliseconds before attempting auto reconnect
      // Set to 0 to disable
      // Typical value 5000 (5 seconds)
      reconnect_delay: 5000,

      // Will log diagnostics on console
      debug: false
    };

    this.stompService.config = stompConfig;
    this.stompService.initAndConnect();
  }

  public close(): void {
    if (this.temperatureSubscription) {
      this.temperatureSubscription.unsubscribe();
    }
    this.stompService.disconnect();
  }

  private publishTemperature(message: MachineTemperature): void {
    this.queue.next(message);
  }

  private publishAlarm(message: MachineAlarm): void {
    this.queueAlarms.next(message);
  }

}
