import {Pipe, PipeTransform} from '@angular/core';

@Pipe({name: 'keysValues'})
export class KeysValuesPipe implements PipeTransform {
  transform(value, args: any[]): { key: any, value: any }[] {
    const keys = [];
    Object.keys(value).forEach(key => keys.push({key: key, value: value[key]}));
    return keys;
  }
}
