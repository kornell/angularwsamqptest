import {Pipe, PipeTransform} from '@angular/core';

@Pipe({name: 'keys'})
export class KeysPipe implements PipeTransform {
  transform(value, args: any[]): any[] {
    const keys = [];
    Object.keys(value).forEach(key => keys.push(key));
    return keys;
  }
}
