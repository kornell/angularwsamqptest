import {Component, OnDestroy} from '@angular/core';
import {TranslateService} from 'ng2-translate';
import {WebSocketService} from '../../shared/service/WebSocketService';
import {Chart} from 'angular-highcharts';
import {DataPoint, PointObject, SeriesObject} from 'highcharts';
import {isNullOrUndefined} from 'util';
import {
  MachineAlarm,
  MachineTemperature,
  WebSocketRabbitMQService
} from '../../shared/service/WebSocketRabbitMQService';
import {Subscription} from 'rxjs/Subscription';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnDestroy {

  pieChart: Chart;

  temperatureChart: Chart;

  private localWsSubscription: Subscription;

  private queueSubscription: Subscription;

  private alarmSubscription: Subscription;

  private alarmMachines: { [key: string]: boolean };

  constructor(private webSocketService: WebSocketService,
              private webSocketRabbitMQService: WebSocketRabbitMQService,
              public t: TranslateService) {

    this.initializePieChart();
    this.initializeTemperatureChart();
    this.alarmMachines = {};
  }

  get pieChartDataLength(): number {
    return isNullOrUndefined(this.pieChart) ? 0 : this.pieChart.ref.series[0].data.length;
  }

  get temperatureChartDataLength(): number {
    return isNullOrUndefined(this.temperatureChart) ? 0 : this.temperatureChart.ref.series[0].data.length;
  }

  private initializePieChart(): void {
    this.pieChart = new Chart({
      chart: {
        type: 'pie'
      },
      title: {
        text: this.t.instant('home.chart.#001.label')
      },
      credits: {
        enabled: false
      },
      series: [{
        name: this.t.instant('home.chart.#001.label'),
        data: []
      }],
      xAxis: {
        type: 'category'
      },
      tooltip: {
        crosshairs: {
          width: 1,
          color: '#3f51b5',
          dashStyle: 'solid'
        },
        shared: true
      }
    });

    this.localWsSubscription = this.webSocketService.socket.subscribe((message: MessageEvent) => {
      const data: any = JSON.parse(message.data);
      const series: SeriesObject = this.pieChart.ref.series[0];
      series.setData(<DataPoint[]>data.message);
    });

  }

  private initializeTemperatureChart(): void {
    this.temperatureChart = new Chart({
      chart: {
        type: 'bar'
      },
      title: {
        text: this.t.instant('home.chart.#002.label')
      },
      credits: {
        enabled: false
      },
      series: [{
        name: this.t.instant('home.chart.#002.label'),
        data: []
      }],
      xAxis: {
        type: 'category'
      },
      tooltip: {
        crosshairs: {
          width: 1,
          color: '#3f51b5',
          dashStyle: 'solid'
        },
        shared: true
      }
    });

    this.alarmSubscription = this.webSocketRabbitMQService.queueAlarms.subscribe((alarmEvent: MachineAlarm) => {
      switch (alarmEvent.state) {
        case 'ERROR':
          this.alarmMachines[alarmEvent.name] = true;
          break;
        case 'OK':
          this.alarmMachines[alarmEvent.name] = false;
          break;
      }
    });
    this.queueSubscription = this.webSocketRabbitMQService.queue
      .subscribe((temperatureEvent: MachineTemperature) => {
        const data: DataPoint = {
          name: temperatureEvent.name,
          y: temperatureEvent.temperature,
          color: '#69c4ff'
        };
        if (this.alarmMachines[data.name] === true) {
          data.color = '#ff746f';
        }
        const series: SeriesObject = this.temperatureChart.ref.series[0];
        const seriesData: PointObject[] = series.data;
        const updatePoint = seriesData.find(pointObject => pointObject.name === data.name);
        if (!isNullOrUndefined(updatePoint)) {
          updatePoint.update(data);
        } else {
          series.addPoint(data);
        }
      });
  }

  ngOnDestroy(): void {
    if (this.localWsSubscription) {
      this.localWsSubscription.unsubscribe();
    }
    if (this.queueSubscription) {
      this.queueSubscription.unsubscribe();
    }
    if (this.alarmSubscription) {
      this.alarmSubscription.unsubscribe();
    }
  }

}
