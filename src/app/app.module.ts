import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppComponent} from './app.component';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {
  MatButtonModule,
  MatCardModule,
  MatCheckboxModule,
  MatDialogModule,
  MatExpansionModule,
  MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatProgressSpinnerModule,
  MatSelectModule,
  MatSidenavModule,
  MatSnackBarModule,
  MatSortModule,
  MatTableModule,
  MatToolbarModule
} from '@angular/material';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {ReactiveFormsModule} from '@angular/forms';
import {RouterModule, Routes} from '@angular/router';
import {HomeComponent} from './components/home/home.component';
import {AuthService} from './shared/service/AuthService';
import {UnauthorizedComponent} from './components/unauthorized/unauthorized.component';
import {APIInterceptor} from './shared/interceptor/APIInterceptor';
import * as moment from 'moment';
import {KeysPipe} from './shared/pipe/KeysPipe';
import {KeysValuesPipe} from './shared/pipe/KeysValuesPipe';
import {TranslateLoader, TranslateModule, TranslateStaticLoader} from 'ng2-translate';
import {Http, HttpModule} from '@angular/http';
import {WebSocketService} from './shared/service/WebSocketService';
import {ChartModule} from 'angular-highcharts';
import {WebSocketRabbitMQService} from './shared/service/WebSocketRabbitMQService';
import {StompRService} from '@stomp/ng2-stompjs';

moment.locale('ru');

const appRoutes: Routes = [
  {path: '', data: {inMenu: false}, redirectTo: '/', pathMatch: 'full'},
  {path: '', data: {inMenu: false}, component: UnauthorizedComponent},
  {path: '**', data: {inMenu: false}, component: UnauthorizedComponent}
];


@NgModule({
  declarations: [
    KeysPipe,
    KeysValuesPipe,
    AppComponent,
    UnauthorizedComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    MatSidenavModule,
    MatGridListModule,
    MatButtonModule,
    MatCardModule,
    MatInputModule,
    MatListModule,
    MatToolbarModule,
    MatDialogModule,
    MatIconModule,
    MatCheckboxModule,
    MatSelectModule,
    MatTableModule,
    MatSortModule,
    MatSnackBarModule,
    MatExpansionModule,
    MatProgressSpinnerModule,
    ChartModule,
    RouterModule.forRoot(appRoutes),

    HttpModule,
    TranslateModule.forRoot({
      provide: TranslateLoader,
      useFactory: (http: Http) => new TranslateStaticLoader(http, '/assets/i18n', '.json'),
      deps: [Http]
    })
  ],
  entryComponents: [
    UnauthorizedComponent,
    HomeComponent
  ],
  providers: [
    AuthService,
    WebSocketService,
    WebSocketRabbitMQService,
    StompRService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: APIInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
