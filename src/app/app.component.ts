import {Component, OnDestroy} from '@angular/core';
import {AuthEvent, AuthService} from './shared/service/AuthService';
import {MatDialog, MatSnackBar} from '@angular/material';
import {NavigationEnd, Route, Router, Routes} from '@angular/router';
import {HomeComponent} from './components/home/home.component';
import {UnauthorizedComponent} from './components/unauthorized/unauthorized.component';
import {TranslateService} from 'ng2-translate';
import {WebSocketService} from './shared/service/WebSocketService';
import {isNullOrUndefined} from 'util';
import {MachineAlarm, WebSocketRabbitMQService} from './shared/service/WebSocketRabbitMQService';
import {Subscription} from 'rxjs/Subscription';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnDestroy {

  currentRoute: Route;

  private alarmSubscription: Subscription;

  constructor(public authService: AuthService,
              public t: TranslateService,
              private webSocketService: WebSocketService,
              private webSocketRabbitMQService: WebSocketRabbitMQService,
              private dialog: MatDialog,
              private router: Router,
              private snackBar: MatSnackBar) {

    // Subscribe on login events
    this.authService.onAuthEvent.subscribe((event: AuthEvent) => {
      switch (event) {
        case 'verificationSuccess':
          // Perform a connection to WS server
          this.webSocketService.connect();
          this.webSocketRabbitMQService.connect();
          this.alarmSubscription = webSocketRabbitMQService.queueAlarms.subscribe((alarmEvent: MachineAlarm) => {
            switch (alarmEvent.state) {
              case 'ERROR':
                const overheat = alarmEvent.temperature - alarmEvent.limit;
                this.snackBar.open(
                  this.t.instant(
                    'snack.machine-alarm-error',
                    {
                      name: alarmEvent.name,
                      temperature: alarmEvent.temperature,
                      overheat: overheat
                    }
                  ),
                  this.t.instant('snack.close'),
                  {
                    duration: 3000,
                    verticalPosition: 'top'
                  }
                );
                break;
              case 'OK':
                this.snackBar.open(
                  this.t.instant('snack.machine-alarm-ok'),
                  this.t.instant('snack.close'),
                  {
                    duration: 3000,
                    verticalPosition: 'top'
                  }
                );
                break;
            }
          });
          // Prepare router and custom menu on auth success
          this.prepareRouter();
          this.router.navigate(['/home']);
          break;
        case 'verificationFailure':
          this.webSocketService.close();
          this.webSocketRabbitMQService.close();
          if (this.alarmSubscription) {
            this.alarmSubscription.unsubscribe();
          }
          this.router.navigate(['/error']);
          break;
      }
    });

    // Update currentRoute on change
    this.router.events.subscribe(navigationEvent => {
        if (!(navigationEvent instanceof NavigationEnd)) {
          return;
        }
        this.currentRoute = this.routes.find((routeItem) => ('/' + routeItem.path) === navigationEvent.url);
      }
    );

    // Set default language (references to json language file under ./src/assets/i18n)
    t.setDefaultLang('en-EN');
    t.use('en-EN');
  }

  ngOnDestroy(): void {
    this.webSocketService.close();
    this.webSocketRabbitMQService.close();
    if (this.alarmSubscription) {
      this.alarmSubscription.unsubscribe();
    }
  }

  prepareRouter(): void {
    const config: Routes = [
      {path: '', redirectTo: '/', pathMatch: 'full'},
      {path: '', component: UnauthorizedComponent}
    ];
    config.push({path: 'home', component: HomeComponent, data: {inMenu: true}});
    config.push({path: 'error', component: UnauthorizedComponent});
    config.push({path: '**', component: UnauthorizedComponent});
    this.router.resetConfig(config);
  }

  get routes(): Route[] {
    return this.router.config
      .filter(route => !isNullOrUndefined(route.data) && route.data.inMenu === true);
  }

}
