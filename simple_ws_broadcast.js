const moment = require('moment');
const ws = require('nodejs-websocket');

const server = ws.createServer(function (connection) {
  connection.on('text', function (message) {
    // Echo service
    notifyClients({
      originalMessage: message
    });
  });
  connection.on('close', function (code, reason) {
    // Connection closed gracefully
    console.log('Connection closed');
  });
  connection.on('error', function (error) {
    // Angular disconnects on F5
    console.log('Connection closed');
    console.log(error);
  });
}).listen(4199);

function notifyClients(result) {
  server.connections.forEach(function (connection) {
    connection.sendText(JSON.stringify(result));
  });
}

setInterval(() => {
  notifyClients({
    type: 'chartValueChange',
    message: [{
      name: 'London',
      y: Math.floor(Math.random() * 100)
    }, {
      name: 'Paris',
      y: Math.floor(Math.random() * 100)
    }, {
      name: 'Dubai',
      y: Math.floor(Math.random() * 100)
    }]
  });
}, 2500);

