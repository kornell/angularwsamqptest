// https://github.com/720kb/node-amqp

// Producer
const nodeAmqp = require('node-amqp')
  , Task = nodeAmqp.Task
  , task = new Task({
  'host': 'amqp://guest:guest@127.0.0.1:5672',
  'queueName': 'temperature',
  // socket options used for example to configure ssl. Reference for this can be found at http://www.squaremobius.net/amqp.node/channel_api.html#connect
  'socketOptions': {}
});

// Define machines
const machines = [
  {
    name: 'Ironer #1',
    temperature: randomInteger(30, 35)
  },
  {
    // Broken Ironer
    name: 'Ironer #2',
    temperature: randomInteger(5, 10)
  },
  {
    name: 'Sorter #1',
    temperature: randomInteger(30, 35)
  },
  {
    name: 'Press #1',
    temperature: randomInteger(30, 35)
  }
];

const temperatureLimit = 60;
const powerOffLimit = 100;

setInterval(() => {
  // Chose random machine
  const machineAffectedIndex = randomInteger(0, machines.length - 1);
  // Emulate value change event. For broken Ironer higher chances to overheat
  const valueChangedBy = randomInteger(-1, machines[machineAffectedIndex].name === 'Ironer #2' ? 3 : 1);
  // Skip useless events
  if (valueChangedBy === 0) {
    return;
  }
  // Perform local cache change
  machines[machineAffectedIndex].temperature += valueChangedBy;
  // Emulate power-off on critical value
  if (machines[machineAffectedIndex].temperature > powerOffLimit) {
    task.send(JSON.stringify({
      type: 'machineValueAlarm',
      message: Object.assign({limit: temperatureLimit, state: 'OK'}, machines[machineAffectedIndex])
    }));
    machines[machineAffectedIndex].temperature = randomInteger(5, 10);
  }
  // Send an event to the queue
  task.send(JSON.stringify({
    type: 'machineValueChange',
    message: machines[machineAffectedIndex]
  }));
  if (machines[machineAffectedIndex].temperature > temperatureLimit) {
    task.send(JSON.stringify({
      type: 'machineValueAlarm',
      message: Object.assign({limit: temperatureLimit, state: 'ERROR'}, machines[machineAffectedIndex])
    }));
  }
}, 300);

function randomInteger(min, max) {
  let rand = min + Math.random() * (max + 1 - min);
  rand = Math.floor(rand);
  return rand;
}

